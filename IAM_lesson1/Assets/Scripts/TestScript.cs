﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    public string LiquidType;
    public int NumberOfBottles;
    public float BottleVolume;
    public bool isEmpty;
    public GameObject Sasha;
    public float AllLiquid;


    // Start is called before the first frame update
    void Start()
    {
        LiquidType = "Coffee";
        NumberOfBottles = 5;
        BottleVolume = 1.5f;
        isEmpty = false;
        AllLiquid = (float)NumberOfBottles * BottleVolume;
    }

    // Update is called once per frame
    void Update()
    {
        if (isEmpty == false)
        {
            AllLiquid = AllLiquid - 0.025f;
            CheckVolumeOfLiquid();
            Debug.Log(LiquidType + " volume " + AllLiquid + " liters.");
        } 
    }

    void CheckVolumeOfLiquid()
    {
        if (AllLiquid <= 0)
        {
            isEmpty = true;
            Debug.Log(LiquidType + " закончился");
        }

    }
}
