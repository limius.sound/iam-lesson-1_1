﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperFootstepsScript : MonoBehaviour
{
    public AudioSource footstepsAudioSource;
    public AudioClip[] FootstepsAudioClip;
    public int LastIndex;
    public int NewIndex;


    // Start is called before the first frame update
    void Start()
    {
        footstepsAudioSource = gameObject.AddComponent<AudioSource>();
        // footstepsAudioSource.clip = FootstepsAudioClip;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void footstep()
    {
        Randomization();
        footstepsAudioSource.volume = Random.Range(0.8f, 1f);
        footstepsAudioSource.pitch = Random.Range(0.8f, 1.2f);
        footstepsAudioSource.PlayOneShot(FootstepsAudioClip[NewIndex]);
        Debug.Log("Наступил!" + NewIndex);
        LastIndex = NewIndex;
    }

    void Randomization()
    {
        NewIndex = Random.Range(0, FootstepsAudioClip.Length);
        while (NewIndex == LastIndex)
            NewIndex = Random.Range(0, FootstepsAudioClip.Length);
    }
        
       
}
