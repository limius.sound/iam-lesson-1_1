﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayWindSound : MonoBehaviour
{
    public AudioSource windaudiosource;
    public AudioClip windaudioclip;
    // Start is called before the first frame update
    void Start()
    {
        windaudiosource = gameObject.AddComponent<AudioSource>();
        windaudiosource.clip = windaudioclip;
        windaudiosource.loop = true;
        windaudiosource.volume = 0.5f;
        windaudiosource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
